import { Opaque } from '#src/utils/types/opaque';

export type Nickname = Opaque<string, 'Nickname'>;
