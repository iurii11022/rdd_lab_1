export type BackendError = {
  [key: string]: string[];
};
