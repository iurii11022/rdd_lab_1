import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router,
} from '@angular/router';
import { Account } from '#src/app/auth/models/account';
import { currentAccount } from '#src/app/auth/store/auth.selectors';
import { environment } from '#src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { logoutAction } from '#src/app/auth/store/auth.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  loading = true;
  currentAccount$: Observable<Account | null>;
  userList: any[] = [];

  constructor(
    private http: HttpClient,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit() {
    this.router.events.subscribe(event => {
      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });

    this.currentAccount$ = this.store.pipe(select(currentAccount));

    this.currentAccount$.subscribe(account => {
      if (account) {
        this.fetchUserList();
      } else {
        this.router.navigate(['/login']);
      }
    });
  }

  logout() {
    this.store.dispatch(logoutAction());
    this.userList = [];
  }

  fetchUserList() {
    this.http
      .get<Account[]>(`${environment.backendApiUrl}/users`)
      .subscribe(users => {
        this.userList = users;
      });
  }
}
