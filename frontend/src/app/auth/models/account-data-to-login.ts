import { Account } from '#src/app/auth/models/account';

export type AccountDataToLogIn = Account;
