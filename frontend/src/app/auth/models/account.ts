import { Password } from '#src/app/global/models/password';
import { Nickname } from '#src/app/global/models/nickname';

export type Account = {
  password: Password;
  username: Nickname;
};
