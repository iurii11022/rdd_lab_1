import { AccountId } from '#src/app/global/models/account-id';
import { Password } from '#src/app/global/models/password';

export type LogInnedAccountId = {
  account_id: AccountId;
  account_email: Password;
};
