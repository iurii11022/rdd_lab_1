import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import {
  loginAction,
  loginFailureAction,
  loginSuccessAction,
  registerAction,
  registerFailureAction,
  registerSuccessAction,
} from '#src/app/auth/store/auth.actions';
import { AuthHttpClient } from '#src/app/auth/clients/auth.http.client';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Account } from '#src/app/auth/models/account';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private authHttpClient: AuthHttpClient
  ) {}

  register = createEffect(() =>
    this.actions$.pipe(
      ofType(registerAction),
      switchMap(({ accountDataToRegister }) => {
        return this.authHttpClient.register(accountDataToRegister).pipe(
          map((response: HttpResponse<Account>) => {
            return registerSuccessAction({ account: response.body });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              registerFailureAction({ error: errorResponse.error.message })
            );
          })
        );
      })
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginAction),
      switchMap(({ accountDataToLogIn }) => {
        return this.authHttpClient.logIn(accountDataToLogIn).pipe(
          map((response: HttpResponse<Account>) => {
            return loginSuccessAction({ account: response.body });
          }),
          catchError((errorResponse: HttpErrorResponse) => {
            return of(
              loginFailureAction({ error: errorResponse.error.message })
            );
          })
        );
      })
    )
  );

  redirectAfterSubmit$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(loginSuccessAction, registerSuccessAction),
        tap(() => {
          this.router.navigateByUrl('/');
        })
      ),
    { dispatch: false }
  );
}
