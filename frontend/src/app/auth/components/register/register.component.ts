import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { select, Store } from '@ngrx/store';

import { AuthHttpClient } from '../../clients/auth.http.client';
import { Router } from '@angular/router';
import { registerAction } from '../../store/auth.actions';
import { Observable } from 'rxjs';
import { validationErrorSelector } from '#src/app/auth/store/auth.selectors';

@Component({
  selector: 'app-login',
  templateUrl: './register.component.html',
  styleUrls: ['../login-register.component.scss'],
})
export class RegisterComponent implements OnInit {
  form!: UntypedFormGroup;
  backendErrors$!: Observable<string | null>;

  constructor(
    private fb: UntypedFormBuilder,
    private auth: AuthHttpClient,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit() {
    this.initializeForm();
    this.backendErrors$ = this.store.pipe(select(validationErrorSelector));
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      password: ['test', [Validators.required]],
      username: ['nic', [Validators.required]],
    });
  }

  onSubmitRegisterForm(): void {
    const accountDataToRegisterProps = {
      accountDataToRegister: this.form.value,
    };

    this.store.dispatch(registerAction(accountDataToRegisterProps));
  }
}
