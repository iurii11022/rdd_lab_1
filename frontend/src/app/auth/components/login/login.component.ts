import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';

import { select, Store } from '@ngrx/store';

import { Router } from '@angular/router';
import { loginAction, registerAction } from '../../store/auth.actions';
import { Observable } from 'rxjs';
import { validationErrorSelector } from '#src/app/auth/store/auth.selectors';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['../login-register.component.scss'],
})
export class LoginComponent implements OnInit {
  form: UntypedFormGroup;
  backendErrors$!: Observable<string | null>;

  constructor(
    private fb: UntypedFormBuilder,
    private router: Router,
    private store: Store
  ) {
    this.form = fb.group({
      username: ['nic', [Validators.required]],
      password: ['test', [Validators.required]],
    });
  }

  objectKeys = Object.keys;

  ngOnInit() {
    this.initializeForm();
    this.backendErrors$ = this.store.pipe(select(validationErrorSelector));
  }

  private initializeForm(): void {
    this.form = this.fb.group({
      username: ['nic', [Validators.required]],
      password: ['test', [Validators.required]],
    });
  }

  onSubmitLoginForm(): void {
    const accountDataToLogInProps = {
      accountDataToLogIn: this.form.value,
    };

    this.store.dispatch(loginAction(accountDataToLogInProps));
  }
}
