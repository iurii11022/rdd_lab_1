import { createReducer, on } from '@ngrx/store';
import { AuthState } from '#src/app/auth/store/models/auth.state';
import {
  loginFailureAction,
  loginSuccessAction,
  logoutAction,
  registerFailureAction,
  registerSuccessAction,
} from '#src/app/auth/store/auth.actions';

export const initialAuthState: AuthState = {
  account: null,
  error: null,
};

export const authReducer = createReducer(
  initialAuthState,

  on(
    loginSuccessAction,
    (state, action): AuthState => ({
      ...state,
      account: action.account,
      error: null,
    })
  ),

  on(
    registerSuccessAction,
    (state, action): AuthState => ({
      ...state,
      account: action.account,
      error: null,
    })
  ),

  on(
    logoutAction,
    (state, action): AuthState => ({
      ...initialAuthState,
      error: null,
    })
  ),

  on(loginFailureAction, (state, action): AuthState => {
    return { ...state, account: null, error: action.error };
  }),

  on(registerFailureAction, (state, action): AuthState => {
    return { ...state, account: null, error: action.error };
  })
);
