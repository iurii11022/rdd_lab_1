import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState } from '#src/app/auth/store/models/auth.state';
import { authStoreName } from '#src/app/auth/store/models/auth.store-name';

export const selectAuthState = createFeatureSelector<AuthState>(authStoreName);

export const isLoggedIn = createSelector(
  selectAuthState,
  (auth: AuthState) => !!auth.account
);

export const isLoggedOut = createSelector(
  selectAuthState,
  (auth: AuthState) => !auth.account
);

export const username = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.account.username
);

export const currentAccount = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.account
);

export const validationErrorSelector = createSelector(
  selectAuthState,
  (auth: AuthState) => auth.error
);
