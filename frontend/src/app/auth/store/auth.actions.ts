import { createAction, props } from '@ngrx/store';
import { Account } from '../models/account';
import {
  LogInActionType,
  LogOutActionType,
  RegisterActionTypes,
} from '#src/app/auth/store/models/auth.action-types';
import { AccountDataToRegister } from '#src/app/auth/models/account-data-to-register';
import { AccountDataToLogIn } from '#src/app/auth/models/account-data-to-login';

export const loginAction = createAction(
  LogInActionType.LOGIN,
  props<{ accountDataToLogIn: AccountDataToLogIn }>()
);

export const loginSuccessAction = createAction(
  LogInActionType.LOGIN_SUCCESS,
  props<{ account: Account }>()
);

export const loginFailureAction = createAction(
  LogInActionType.LOGIN_FAILURE,
  props<{ error: string }>()
);

export const registerAction = createAction(
  RegisterActionTypes.REGISTER,
  props<{ accountDataToRegister: AccountDataToRegister }>()
);

export const registerSuccessAction = createAction(
  RegisterActionTypes.REGISTER_SUCCESS,
  props<{ account: Account }>()
);

export const registerFailureAction = createAction(
  RegisterActionTypes.REGISTER_FAILURE,
  props<{ error: string }>()
);

export const logoutAction = createAction(LogOutActionType.LOGOUT);
