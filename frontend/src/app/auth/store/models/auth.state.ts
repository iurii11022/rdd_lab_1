import { Account } from '#src/app/auth/models/account';

export type AuthState = {
  account: Account | null;
  error: string | null;
};
