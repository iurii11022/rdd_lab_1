import { authStoreName } from '#src/app/auth/store/models/auth.store-name';

export const RegisterActionTypes = {
  REGISTER: `[${authStoreName}] Register`,
  REGISTER_SUCCESS: `[${authStoreName}] Register Success`,
  REGISTER_FAILURE: `[${authStoreName}] Register Failure`,
} as const;

export const LogInActionType = {
  LOGIN: `[${authStoreName}] Login`,
  LOGIN_SUCCESS: `[${authStoreName}] Login Success`,
  LOGIN_FAILURE: `[${authStoreName}] Login Failure`,
} as const;

export const LogOutActionType = {
  LOGOUT: `[${authStoreName}] Logout`,
  LOGOUT_SUCCESS: `[${authStoreName}] Logout Success`,
  LOGOUT_FAILURE: `[${authStoreName}] Logout Failure`,
} as const;

export const refreshSessionActionType = {
  REFRESH_SESSION: `[${authStoreName}] Refresh Session`,
  REFRESH_SESSION_SUCCESS: `[${authStoreName}] Refresh Session Success`,
  REFRESH_SESSION_FAILURE: `[${authStoreName}] Refresh Session Failure`,
} as const;
