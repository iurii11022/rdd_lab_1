import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '#src/environments/environment';
import { AccountDataToRegister } from '#src/app/auth/models/account-data-to-register';
import { AccountDataToLogIn } from '#src/app/auth/models/account-data-to-login';
import { Account } from '#src/app/auth/models/account';

const apiUrls = {
  REGISTER: `${environment.backendApiUrl}/sign-up`,
  LOGIN: `${environment.backendApiUrl}/log-in`,
  GET_ALL_USERS: `${environment.backendApiUrl}/users`,
} as const;

@Injectable()
export class AuthHttpClient {
  constructor(private http: HttpClient) {}

  register(
    accountDataToRegister: AccountDataToRegister
  ): Observable<HttpResponse<Account>> {
    return this.http.post<Account>(
      apiUrls.REGISTER,
      {
        username: accountDataToRegister.username,
        password: accountDataToRegister.password,
      },
      { observe: 'response' }
    );
  }

  logIn(
    accountDataToLogIn: AccountDataToLogIn
  ): Observable<HttpResponse<Account>> {
    return this.http.post<Account>(
      apiUrls.LOGIN,
      {
        username: accountDataToLogIn.username,
        password: accountDataToLogIn.password,
      },
      { observe: 'response' }
    );
  }

  getAllUsers(): Observable<Account[]> {
    return this.http.get<Account[]>(apiUrls.GET_ALL_USERS);
  }
}
