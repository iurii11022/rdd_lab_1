import * as crypto from 'crypto';
import { BadRequestException, Injectable } from '@nestjs/common';

export type User = {
  username: string;
  password: string;
};

@Injectable()
export class AppService {
  private users: User[] = [];
  private salt = crypto.randomBytes(16).toString('hex');

  public getAllUsers(): User[] {
    return this.users;
  }

  public createNewUser(username: string, passwordToHash: string): User {
    const user = this.users.find((user: User) => user.username === username);

    if (user) {
      throw new BadRequestException('користувач вже існує');
    }
    const password = this.hashSaltedPassword(passwordToHash);
    const newuser = { username, password };
    this.users.push(newuser);

    return newuser;
  }

  public loginUser(username: string, passwordToHash: string): User {
    const password = this.hashSaltedPassword(passwordToHash);

    const user = this.users.find(
      (user: User) => user.username === username && user.password === password,
    );

    if (!user) {
      throw new BadRequestException('невірний логін або пароль');
    }

    return user;
  }

  // без солі
  private hashPassword(password: string): string {
    return crypto.createHash('md5').update(password).digest('hex');
  }

  private hashSaltedPassword(password: string): string {
    const saltedPassword = password + this.salt;
    return crypto.createHash('md5').update(saltedPassword).digest('hex');
  }
}
