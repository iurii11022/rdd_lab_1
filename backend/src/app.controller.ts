import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService, User } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('users')
  getAllusers(): User[] {
    return this.appService.getAllUsers();
  }

  @Post('sign-up')
  createNewUser(@Body() userData: User): User {
    return this.appService.createNewUser(userData.username, userData.password);
  }

  @Post('log-in')
  loginUser(@Body() userData: User): User {
    return this.appService.loginUser(userData.username, userData.password);
  }
}
